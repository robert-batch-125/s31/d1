const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (savedUser, error)=>{
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}

// createUser(req.body);

//retrieve all users
module.exports.allUsers = () =>{

	//Model.method
	return User.find().then( (result, error) => {
			if(error){
				return error
			} else {
				//send [array of objects] as a response
				return result
			}
		})
};


//retrieve a specific user
module.exports.specificUser = (params) =>{
	
	//model.method
	return User.findById(params).then((result) =>{
			
			//send documents as a return
			return result
		})
	};


//update user's info
module.exports.updateUser = (params,reqBody) =>{
	
	//model.method
	return User.findByIdAndUpdate(params, reqBody, {new: true}).then((result, error)=>{
			if (error){
				return error
			} else {
				//return user object
				return result
			}
		})
	};


//delete user
module.exports.deleteUser = (params) =>{

	//model.method
	return User.findByIdAndDelete(params). then((result, error) => {
		if(error){
			return error
		} else {
			return true
		}
	})
};
